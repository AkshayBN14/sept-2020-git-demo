DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS bookcopies;
DROP TABLE IF EXISTS issuerecords;
DROP TABLE IF EXISTS payments;

CREATE TABLE users (user_id INT PRIMARY KEY AUTO_INCREMENT, username VARCHAR(30), email VARCHAR(30), phone VARCHAR(15), password VARCHAR(15), role VARCHAR(15));

CREATE TABLE books (book_id INT PRIMARY KEY AUTO_INCREMENT, bookname VARCHAR(50), author VARCHAR(50), subject VARCHAR(25), price DOUBLE, isbn VARCHAR(16));

CREATE TABLE bookcopies (bookcopy_id INT PRIMARY KEY AUTO_INCREMENT, book_id INT, rack INT, status VARCHAR(20), FOREIGN KEY (book_id) REFERENCES books(book_id));

CREATE TABLE issuerecords (issuerecord_id INT PRIMARY KEY AUTO_INCREMENT, bookcopy_id INT, user_id INT, issue_date DATE, return_duedate DATE, return_date DATE, fine_amount DOUBLE, FOREIGN KEY (bookcopy_id) REFERENCES bookcopies(bookcopy_id), FOREIGN KEY (user_id) REFERENCES users(user_id));

CREATE TABLE payments (payment_id INT PRIMARY KEY AUTO_INCREMENT, user_id INT, amount DOUBLE, pay_type varchar(10), transaction_date DATE, nextpay_duedate DATE, FOREIGN KEY (user_id) REFERENCES users(user_id));